/*jeremie*/
#include "modelise.h"
#include "pile.h"
#include "bt.h"
#include "tableaufc.h"

/*Fonction d'initialisation d'une pile par rapport à la taille de la grille*/

void pile_init(pile* pile, int taille_grille) {

    /*Allocation mémoire nécessaire de la liste d'éléments mémo_pile*/
    
    memo_pile* tab_elements;
    tab_elements = malloc(sizeof (memo_pile) * taille_grille * taille_grille + 1);
    if (tab_elements == NULL) {
        fprintf(stderr, "Probleme allocation memoire durant la création de la pile");
        exit(EXIT_FAILURE);
    }

    /*Allocation mémoire du tableau d'int qui stocke les valeurs filtré d'un élément mémo_pile*/
    
    int i;
    for (i = 0; i < taille_grille * taille_grille; i++) {

        sommet_filtre* liste_sommet_filtre;
        liste_sommet_filtre = malloc(sizeof (struct sommet_filtre) * 2 * (taille_grille - 1));
                
        int j;
        
        for (j = 0; j < 2 * (taille_grille - 1); j++) {
            int* tab_int;
            tab_int = malloc(sizeof (int) *(taille_grille));
            liste_sommet_filtre[j].valeurs_filtrees = tab_int;
            liste_sommet_filtre[j].nb_valeurs_filtrees = 0;
        }

        tab_elements[i].liste_sommets_filtres = liste_sommet_filtre;
        tab_elements[i].indice_domaine = 0;
        tab_elements[i].nb_sommets_filtres = 0;
    }

    pile->elements = tab_elements;
    pile->max_size = taille_grille * taille_grille + 1;
    pile->top = -1;
};

/*Fonction de libération de la mémoire allouée pour la pile*/

void pile_destroy(pile* pile) {
    int i_element;
    for (i_element = 0; i_element < pile->max_size; i_element++) {       
        pile->elements[i_element].liste_sommets_filtres = NULL;
        free(pile->elements[i_element].liste_sommets_filtres);
    }
    pile->elements = NULL;
    free(pile->elements);
    pile->max_size = 0;
    pile->top = -1;
    pile = NULL;
    free(pile);
};

/*Fonction qui teste si une pile est vide (retourne 1) ou non (retourne 0)*/

int pile_is_empty(pile* pile) {
    if (pile->top == -1) {
        return 1;
    }
    return 0;
};

/*Fonction qui teste si une pile est pleine (retourne 1) ou non (retourne 0)*/


int pile_is_full(pile* pile) {
    if (pile->top == (pile->max_size) - 1) {
        return 1;
    }
    return 0;
};

/*Fonction qui ajoute un élément mémo_pile au dessus de la pile*/

void pile_push(pile* pile, memo_pile element) {
    
    /*Voir si c'est possible*/
    
    if (pile_is_full(pile)) {
        fprintf(stderr, "Ajout impossible: La pile est pleine");
        exit(EXIT_FAILURE);
    }
    
    /*Récupération des valeurs*/
    
    pile->elements[++pile->top].indice_domaine = element.indice_domaine;
    pile->elements[pile->top].sommet_courant = element.sommet_courant;
    pile->elements[pile->top].nb_sommets_filtres = element.nb_sommets_filtres;
    int i;
    for (i = 0; i < element.nb_sommets_filtres; i++) {
        pile->elements[pile->top].liste_sommets_filtres[i].sommet_filtre = element.liste_sommets_filtres[i].sommet_filtre;
        int j;
        for (j = 0; j < element.liste_sommets_filtres[i].nb_valeurs_filtrees; j++) {
            pile->elements[pile->top].liste_sommets_filtres[i].valeurs_filtrees[j] = element.liste_sommets_filtres[i].valeurs_filtrees[j];
        }
    }
};

/*Fonction qui récupère l'élément mémo_pile du haut de la pile et "l'enlève"*/

memo_pile pile_pop(pile* pile) {
    if (pile_is_empty(pile)) {
        fprintf(stderr, "Impossible de pop l'element: La pile est vide");
        exit(EXIT_FAILURE);
    }
    return pile->elements[pile->top--];
};

/*Fonction qui récupère l'élément mémo_pile en haut de la pile*/

memo_pile pile_peek(pile* pile) {
    if (pile_is_empty(pile)) {
        fprintf(stderr, "Impossible de peek l'element: La pile est vide");
        exit(EXIT_FAILURE);
    }
    return pile->elements[pile->top];
};


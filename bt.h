/*jeremie*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include "pile.h"
#include "modelise.h"

#ifndef BT_H
#define	BT_H

int is_consistent(sommet* sommet_courant, int valeur);

void resolve_fut_bt(sommet* file_sommet, struct pile* pile, int taille_grille);

void affiche_grille(sommet* file_sommet, int taille_grille);

#endif	/* BT_H */


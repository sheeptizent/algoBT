/*jeremie*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include "pile.h"
#include "modelise.h"

#ifndef FC_H
#define	FC_H

void resolve_fut_fc(sommet* file_sommet, struct pile* pile, int taille_grille);

int filtrage(int valeur, memo_pile* memo_pile, int taille_grille);

void restauration_domaine(memo_pile* memo_pile);

int valeur_peek_fc(int* tab, int taille_grille, int indice);

sommet* heuristique_dom_s_deg(sommet* fille_sommet, int nombre_sommet);

sommet* heuristique_cross_perso(sommet* file_sommet, int nombre_sommet, int taille_grille, int* num_sommet_cross, int* flag_cross, int* sommet_num_min_ligne, int* sommet_num_max_ligne);

void prepare_cross_perso(int* sommet_num_min_ligne, int* sommet_num_max_ligne, int* num_sommet_cross, int taille_grille);

#endif	/* FC_H */


<h1> How to use </h1>
<br>
<p> Please download futoshiki file (1) and a grid (.fut files) (2) to solve </p>
<p> Open a terminal and pw in the folder that contains the files then type: </p>
<p> ./futoshiki [arg1] [arg2] </p>
<p> With arg1: the name of a .fut file </p>
<p> With arg2: one of the following words [btnormal | fcdegdom | fccross] (3) </p>
<p> Please let the software solve or find that the grid is unsolvable </p>
<br>
<p> Example for windows: " ./futoshiki_win.exe grille1_5x5.fut btnormal " </p>
<br>
<p> (1) Warning, file futoshiki is compiled for linux OS; </p>
<p> For Windows please dowload futoshiki_win.exe </p>
<br>
<p> (2) You can also create a new grid leaning you on an existing grid </p>
<br>
<p> (3) Notice that is faster to use fcdegdom and fccross for big and/or complex grid </p>
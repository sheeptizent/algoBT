/*jeremie*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include "modelise.h"
#include "pile.h"
#include "tableaufc.h"
#include "bt.h"
#include "fc.h"
#include "fcperso.h"

int main(int argc, char *argv[]) {

    /*test from ide, uncomment following and run*/
    // argc = 3;
    // argv[2] = "fc";
    
    /*Controle nombre d'arguments passés*/
    
    if (argc < 3) {
        fprintf(stderr, "Veuillez passer:\nEn argument 1: un fichier .fut\nEn argument 2: le mode de résolution btnormal, fcdomdeg ou fccross\n");
        exit(EXIT_FAILURE);
    }
    
    /*Validité argv2*/

    if (!(!strcmp(argv[2], "btnormal") || !strcmp(argv[2], "fcdomdeg") || !strcmp(argv[2], "fccross"))){
        fprintf(stderr, "Mode de résolution incorect. Veuillez passer:\nEn argument 2: le mode de résolution btnormal, fcdomdeg ou fccross\n");
        exit(EXIT_FAILURE);
    }  
    
    /*Ouverture du fichier*/
    
    FILE* fichier = NULL;
    fichier = ouvrir_fichier(argc, argv[1]);

    /*Get taille de la grille*/
    
    int taille_grille = (int) fgetc(fichier) - 48;

    /*Instanciation tableau de sommets*/
    
    sommet tableau_de_sommets[taille_grille * taille_grille];
    tableau_de_sommets_init(tableau_de_sommets, taille_grille);

    /*Analyse des données du fichier pour initialiser le tableau de sommets*/
    
    modelisation_donnees(tableau_de_sommets, fichier, taille_grille);

    /*Fermeture fichier*/
    
    fclose(fichier);
    
    /*Instanciation de la pile*/
    
    pile pile_bt;
    pile_init(&pile_bt, taille_grille);
      
    /*Sélection du mode de résolution*/
    
    if (!strcmp(argv[2], "btnormal")) {
        printf("Bactrack sur %s\n", argv[1]);
        
        short_domaine_bt(tableau_de_sommets, taille_grille);
        resolve_fut_bt(tableau_de_sommets, &pile_bt, taille_grille);
    }
    
    if (!strcmp(argv[2], "fcdomdeg")) {
        printf("Forward Checking heuristique dom/deg sur %s\n", argv[1]);
        
        resolve_fut_fc(tableau_de_sommets, &pile_bt, taille_grille);
    }
    
    if (!strcmp(argv[2], "fccross")) {
        printf("Forward Checking heuristique cross sur %s\n", argv[1]);
        
        resolve_fut_fc_perso(tableau_de_sommets, &pile_bt, taille_grille);
    }

    /*Libération mémoire*/
    
    pile_destroy(&pile_bt);
    
    tableau_de_sommets_destroy(tableau_de_sommets, taille_grille);

    exit(0);
}


/*jeremie*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include "modelise.h"
#include "tableaufc.h"

#ifndef PILE_H
#define	PILE_H

typedef struct pile{
    memo_pile *elements;
    int max_size;
    int top;    
} pile;

int pile_is_empty(pile* pile);

int pile_is_full(pile* pile);

memo_pile pile_peek(pile* pile);

memo_pile pile_pop(pile* pile);

void pile_push(pile* pile, memo_pile element);

void pile_init(pile* pile, int taille_grille);

void pile_destroy(pile* pile);

#endif	/* PILE_H */


/*jeremie*/
#include "modelise.h"
#include "pile.h"
#include "bt.h"
#include "tableaufc.h"

/*Fonction d'ouverture du fichier*/

FILE* ouvrir_fichier(int argc, char * argv1) {

    /*test from ide, uncomment following and run*/
    //argc = 2;
    //argv1 = "grille2_8x8.fut";

    /*Controle du fichier passé en argv1*/
    
    if (argc < 2) {
        fprintf(stderr, "Veuillez passer:\nEn argument 1: un fichier .fut\nEn argument 2: le mode de résolution btnormal, fcdomdeg ou fccross\n");
        exit(EXIT_FAILURE);
    }
    
    /*Ouverture du fichier*/
    
    FILE* file = fopen(argv1, "r");
    
    /*Controle de l'extension du fichier*/
    
    if (file != NULL) {
        if ((argv1[strlen(argv1) - 4] != '.') || (argv1[strlen(argv1) - 3] != 'f') || (argv1[strlen(argv1) - 2] != 'u') || (argv1[strlen(argv1) - 1] != 't')) {
            fprintf(stderr, "Extension de fichier non reconnue\n");
            exit(EXIT_FAILURE);
        }
        return file;
    } else {
        fprintf(stderr, "Impossible de trouver le fichier\n");
        exit(EXIT_FAILURE);
    }
};

/*Fonction de traitement du fichier pour la modélisation*/

void modelisation_donnees(sommet* tab, FILE* fichier, int taille_grille) {

    /*init*/
    
    char caractere;
    int count_ligne = 0;
    int count_ligne_char = 0;
    int indice_tab = 0;
    int count_ligne_sommet = 0;
    int indice_chaine = 0;
    char chaine[taille_grille * (taille_grille + taille_grille - 1)];
    fgetc(fichier);

    /*Traitement caractère par caractère jusque la fin du fichier*/
    
    while (caractere != EOF) {

        caractere = fgetc(fichier);

        /*Traitement ligne paires avec sommets*/
        /*Numérotation des lignes paires avec les sommets et initialisation du compte des caractère pour la ligne*/
        
        if (caractere == '\n') {
            count_ligne++;
            if (count_ligne % 2 == 0) {
                count_ligne_sommet++;
                count_ligne_char = 0;
            }
        }

        if ((caractere != EOF) && (caractere != '\n') && (caractere != '\r')) {

            /*Si la ligne est paire*/
            
            if (count_ligne % 2 == 0) {
                
                /*Si le caractère est un sommet*/
                
                if (count_ligne_char % 2 == 0) {
                    
                    /*Récupération du caractère et cast en int*/
                    int valeur = (int) caractere - 48;
                    tab[indice_tab].valeur = valeur;

                    /*Traitement du dommaine d'un sommet renseigné*/
                    
                    if (valeur != 0) {
                        tab[indice_tab].is_known = 1;
                        int n;
                        for (n = 0; n < taille_grille; n++) {
                            if (tab[indice_tab].domaine[n] != valeur) {
                                tab[indice_tab].domaine[n] = 0;
                            }
                            tab[indice_tab].card_domaine = 1;
                        }
                    }

                    /*Traitement des domaines des sommets non renseignés*/
                    /*Ajout des sommets à la liste d'adjacences de contrainte: est différents*/
                    
                    int i = 0;
                    int j = 0;
                    
                    /*Pour les indices i: traitement de la ligne*/
                    /*Pour les indices j: trairement de la colonne*/
                    
                    for (i = (count_ligne_sommet * taille_grille); i < indice_tab; i++) {
                        if (valeur != 0) {
                            int k = 0;
                            for (k = 0; k < taille_grille; k++) {
                                if (tab[i].domaine[k] == valeur) {
                                    tab[i].domaine[k] = 0;
                                    tab[i].card_domaine--;
                                }
                            }
                        }
                        tab[indice_tab].liste_sommets_diff[j] = &tab[i];
                        tab[indice_tab].coeff_contraintes++;
                        j++;
                    }

                    for (i = (indice_tab + 1); i < ((count_ligne_sommet * taille_grille) + (taille_grille)); i++) {
                        if (valeur != 0) {
                            int k = 0;
                            for (k = 0; k < taille_grille; k++) {
                                if (tab[i].domaine[k] == valeur) {
                                    tab[i].domaine[k] = 0;
                                    tab[i].card_domaine--;
                                }
                            }
                        }
                        tab[indice_tab].liste_sommets_diff[j] = &tab[i];
                        tab[indice_tab].coeff_contraintes++;
                        j++;
                    }

                    int k = 0;
                    for (k = 0; k < count_ligne_sommet; k++) {
                        int l = (indice_tab % taille_grille) + (k * taille_grille);
                        if (valeur != 0) {
                            int k = 0;
                            for (k = 0; k < taille_grille; k++) {
                                if (tab[l].domaine[k] == valeur) {
                                    tab[l].domaine[k] = 0;
                                    tab[l].card_domaine--;
                                }
                            }
                        }
                        tab[indice_tab].liste_sommets_diff[j] = &tab[l];
                        tab[indice_tab].coeff_contraintes++;
                        j++;
                    }
                    for (k = (count_ligne_sommet + 1); k < taille_grille; k++) {
                        int l = (indice_tab % taille_grille) + (k * taille_grille);
                        if (valeur != 0) {
                            int k = 0;
                            for (k = 0; k < taille_grille; k++) {
                                if (tab[l].domaine[k] == valeur) {
                                    tab[l].domaine[k] = 0;
                                    tab[l].card_domaine--;
                                }
                            }
                        }
                        tab[indice_tab].liste_sommets_diff[j] = &tab[l];
                        tab[indice_tab].coeff_contraintes++;
                        j++;
                    }

                    indice_tab++;
                    count_ligne_char++;
                }

                /*Si caractère de la ligne paire est une relation on ajoute dans la chaine pour traitement futur*/
                
                if (count_ligne_char % 2 == 1) {
                    caractere = fgetc(fichier);
                    chaine[indice_chaine] = caractere;
                    indice_chaine++;
                    count_ligne_char++;
                }
            }

            /*Si la ligne est impaire (ligne de relation interligne*/
            /*On ajoute le caractère à la ligne pour traitement futur*/
            
            if ((count_ligne % 2) == 1) {
                chaine[indice_chaine] = caractere;
                indice_chaine++;
                count_ligne_char++;
            }
        }
    }

    /*Traitement des liste d'adjacences des relations est inférieur et est supérieur*/
    
    int c;
    int count_lignechar = 0;
    for (c = 0; c < (taille_grille * (taille_grille + taille_grille - 1)); c++) {

        /*Compte des lignes de ralation (lignes '<', '>', ' ' et interlignes '^', 'v' , '.')*/
        
        if (c % taille_grille == 0) {
            count_lignechar++;
        }

        /*Numéro sommet à gauche de la relation ('<', '>', ' ') ou numéro sommet en bas de la relation ('^', 'v' , '.')*/
        
        int num = c % taille_grille + ((count_lignechar)) / 2 * taille_grille;

        
        /*Traitement de chaque cas de caractère*/
        
        if (chaine[c] == '>') {
            tab[num].coeff_contraintes +=taille_grille;
            tab[num+1].coeff_contraintes +=taille_grille;
            int card_diff_s1 = tab[num].card_liste_sommet_diff;
            int card_diff_s2 = tab[num + 1].card_liste_sommet_diff;
            int card_inf = tab[num].card_liste_sommet_inf;
            int card_sup = tab[num + 1].card_liste_sommet_sup;
            tab[num].liste_sommets_inf[card_inf] = &tab[num + 1];
            tab[num].card_liste_sommet_inf++;
            tab[num + 1].liste_sommets_sup[card_sup] = &tab[num];
            tab[num + 1].card_liste_sommet_sup++;
            enlever_p_sommet(tab[num].liste_sommets_diff, card_diff_s1, &tab[num + 1]);
            tab[num].card_liste_sommet_diff--;
            enlever_p_sommet(tab[num + 1].liste_sommets_diff, card_diff_s2, &tab[num]);
            tab[num + 1].card_liste_sommet_diff--;

            if ((tab[num].valeur == 0) && (tab[num].domaine[0] != 0)) {
                tab[num].domaine[0] = 0;
                tab[num].card_domaine--;
            }

            if ((tab[num + 1].valeur == 0) && (tab[num + 1].domaine[taille_grille - 1] != 0)) {
                tab[num + 1].domaine[taille_grille - 1] = 0;
                tab[num + 1].card_domaine--;
            }

            if ((tab[num].valeur != 0) && (tab[num + 1].valeur == 0)) {
                int i;
                for (i = 0; i < taille_grille; i++) {
                    if ((tab[num].valeur <= tab[num + 1].domaine[i]) && (tab[num + 1].domaine[i] != 0)) {
                        tab[num + 1].domaine[i] = 0;
                        tab[num + 1].card_domaine--;
                    }
                }
            }

            if ((tab[num + 1].valeur != 0) && (tab[num].valeur == 0)) {
                int i;
                for (i = 0; i < taille_grille; i++) {
                    if ((tab[num].domaine[i] <= tab[num + 1].valeur) && (tab[num].domaine[i] != 0)) {
                        tab[num].domaine[i] = 0;
                        tab[num].card_domaine--;
                    }
                }
            }
        }

        if (chaine[c] == '<') {
            tab[num].coeff_contraintes +=taille_grille;
            tab[num+1].coeff_contraintes +=taille_grille;
            int card_diff_s1 = tab[num].card_liste_sommet_diff;
            int card_diff_s2 = tab[num + 1].card_liste_sommet_diff;
            int card_inf = tab[num + 1].card_liste_sommet_inf;
            int card_sup = tab[num].card_liste_sommet_sup;
            tab[num + 1].liste_sommets_inf[card_inf] = &tab[num];
            tab[num + 1].card_liste_sommet_inf++;
            tab[num].liste_sommets_sup[card_sup] = &tab[num + 1];
            tab[num].card_liste_sommet_sup++;
            enlever_p_sommet(tab[num + 1].liste_sommets_diff, card_diff_s1, &tab[num]);
            tab[num + 1].card_liste_sommet_diff--;
            enlever_p_sommet(tab[num].liste_sommets_diff, card_diff_s2, &tab[num + 1]);
            tab[num].card_liste_sommet_diff--;

            if ((tab[num + 1].valeur == 0) && (tab[num + 1].domaine[0] != 0)) {
                tab[num + 1].domaine[0] = 0;
                tab[num + 1].card_domaine--;
            }

            if ((tab[num].valeur == 0) && (tab[num].domaine[taille_grille - 1] != 0)) {
                tab[num].domaine[taille_grille - 1] = 0;
                tab[num].card_domaine--;
            }

            if ((tab[num].valeur != 0) && (tab[num + 1].valeur == 0)) {
                int i;
                for (i = 0; i < taille_grille; i++) {
                    if ((tab[num].valeur >= tab[num + 1].domaine[i]) && (tab[num + 1].domaine[i] != 0)) {
                        tab[num + 1].domaine[i] = 0;
                        tab[num + 1].card_domaine--;
                    }
                }
            }

            if ((tab[num + 1].valeur != 0) && (tab[num].valeur == 0)) {
                int i;
                for (i = 0; i < taille_grille; i++) {
                    if ((tab[num].domaine[i] >= tab[num + 1].valeur) && (tab[num].domaine[i] != 0)) {
                        tab[num].domaine[i] = 0;
                        tab[num].card_domaine--;
                    }
                }
            }
        }

        if (chaine[c] == 'v') {
            tab[num].coeff_contraintes +=taille_grille;
            tab[num - taille_grille].coeff_contraintes +=taille_grille;
            int card_diff_s1 = tab[num - taille_grille].card_liste_sommet_diff;
            int card_diff_s2 = tab[num].card_liste_sommet_diff;
            int card_inf = tab[num - taille_grille].card_liste_sommet_inf;
            int card_sup = tab[num].card_liste_sommet_sup;
            tab[num - taille_grille].liste_sommets_inf[card_inf] = &tab[num];
            tab[num - taille_grille].card_liste_sommet_inf++;
            tab[num].liste_sommets_sup[card_sup] = &tab[num - taille_grille];
            tab[num].card_liste_sommet_sup++;
            enlever_p_sommet(tab[num - taille_grille].liste_sommets_diff, card_diff_s1, &tab[num]);
            tab[num - taille_grille].card_liste_sommet_diff--;
            enlever_p_sommet(tab[num].liste_sommets_diff, card_diff_s2, &tab[num - taille_grille]);
            tab[num].card_liste_sommet_diff--;

            if ((tab[num - taille_grille].valeur == 0) && (tab[num - taille_grille].domaine[0] != 0)) {
                tab[num - taille_grille].domaine[0] = 0;
                tab[num - taille_grille].card_domaine--;
            }

            if ((tab[num].valeur == 0) && (tab[num].domaine[taille_grille - 1] != 0)) {
                tab[num].domaine[taille_grille - 1] = 0;
                tab[num].card_domaine--;
            }

            if ((tab[num - taille_grille].valeur != 0) && (tab[num].valeur == 0)) {
                int i;
                for (i = 0; i < taille_grille; i++) {
                    if ((tab[num - taille_grille].valeur <= tab[num].domaine[i]) && (tab[num].domaine[i] != 0)) {
                        tab[num].domaine[i] = 0;
                        tab[num].card_domaine--;
                    }
                }
            }

            if ((tab[num].valeur != 0) && (tab[num - taille_grille].valeur == 0)) {
                int i;
                for (i = 0; i < taille_grille; i++) {
                    if ((tab[num - taille_grille].domaine[i] <= tab[num].valeur) && (tab[num - taille_grille].domaine[i] != 0)) {
                        tab[num - taille_grille].domaine[i] = 0;
                        tab[num - taille_grille].card_domaine--;
                    }
                }
            }

        }

        if (chaine[c] == '^') {
            tab[num].coeff_contraintes +=taille_grille;
            tab[num+1].coeff_contraintes +=taille_grille;
            int card_diff_s1 = tab[num - taille_grille].card_liste_sommet_diff;
            int card_diff_s2 = tab[num].card_liste_sommet_diff;
            int card_inf = tab[num].card_liste_sommet_inf;
            int card_sup = tab[num - taille_grille].card_liste_sommet_sup;
            tab[num].liste_sommets_inf[card_inf] = &tab[num - taille_grille];
            tab[num].card_liste_sommet_inf++;
            tab[num - taille_grille].liste_sommets_sup[card_sup] = &tab[num];
            tab[num - taille_grille].card_liste_sommet_sup++;
            enlever_p_sommet(tab[num].liste_sommets_diff, card_diff_s1, &tab[num - taille_grille]);
            tab[num].card_liste_sommet_diff--;
            enlever_p_sommet(tab[num - taille_grille].liste_sommets_diff, card_diff_s2, &tab[num]);
            tab[num - taille_grille].card_liste_sommet_diff--;

            if ((tab[num].valeur == 0) && (tab[num].domaine[0] != 0)) {
                tab[num].domaine[0] = 0;
                tab[num].card_domaine--;
            }

            if ((tab[num - taille_grille].valeur == 0) && (tab[num - taille_grille].domaine[taille_grille - 1] != 0)) {
                tab[num - taille_grille].domaine[taille_grille - 1] = 0;
                tab[num - taille_grille].card_domaine--;
            }

            if ((tab[num - taille_grille].valeur != 0) && (tab[num].valeur == 0)) {
                int i;
                for (i = 0; i < taille_grille; i++) {
                    if ((tab[num - taille_grille].valeur >= tab[num].domaine[i]) && (tab[num].domaine[i] != 0)) {
                        tab[num].domaine[i] = 0;
                        tab[num].card_domaine--;
                    }
                }
            }

            if ((tab[num].valeur != 0) && (tab[num - taille_grille].valeur == 0)) {
                int i;
                for (i = 0; i < taille_grille; i++) {
                    if ((tab[num - taille_grille].domaine[i] >= tab[num].valeur) && (tab[num - taille_grille].domaine[i] != 0)) {
                        tab[num - taille_grille].domaine[i] = 0;
                        tab[num - taille_grille].card_domaine--;
                    }
                }
            }
        }
    }
};

/*Fonction pour limiter les calculs lors du choix et du stockage dans la pile d'une valeur pour une variable*/

void short_domaine_bt(sommet* lst_sommet, int taille_grille) {
    int it;
    for (it = 0; it < taille_grille * taille_grille; it++) {
        int i = 0;
        int cursor = 0;
        int tmp;
        
        /*Les valeurs utiles des domaines sont mises au début du tableau*/
        
        for (i = 0; i < taille_grille; i++) {
            if (lst_sommet[it].domaine[i] != 0) {
                tmp = lst_sommet[it].domaine[i];
                lst_sommet[it].domaine[i] = lst_sommet[it].domaine[cursor];
                lst_sommet[it].domaine[cursor] = tmp;
                cursor++;
            } 
        }
    }
};
/*jeremie*/
#include "modelise.h"
#include "pile.h"
#include "bt.h"
#include "tableaufc.h"

/*Fonction pour tester si une affectation est consistante*/
/*Retourne 0 si l'affectation est inconsistante et 1 sinon*/

int is_consistent(sommet* sommet_courant, int valeur) {
    int card_diff = sommet_courant->card_liste_sommet_diff;
    int card_inf = sommet_courant->card_liste_sommet_inf;
    int card_sup = sommet_courant->card_liste_sommet_sup;

    int j;
    int k;
    int i;

    for (j = 0; j < card_inf; j++) {
        if ((valeur <= sommet_courant->liste_sommets_inf[j]->valeur) && (sommet_courant->liste_sommets_inf[j]->valeur != 0)) {
            return 0;
        }
    }

    for (k = 0; k < card_sup; k++) {
        if ((valeur >= sommet_courant->liste_sommets_sup[k]->valeur) && (sommet_courant->liste_sommets_sup[k]->valeur != 0)) {
            return 0;
        }
    }

    for (i = 0; i < card_diff; i++) {
        if (valeur == sommet_courant->liste_sommets_diff[i]->valeur) {
            return 0;
        }
    }

    return 1;
};

/*Fonction de résolution de la grille futoshiki avec backtrack simple*/

void resolve_fut_bt(sommet* file_sommet, pile* pile, int taille_grille) {

    /*Init*/
    
    /*Compte en base milliard*/
    int count_sommet_instancie_millions = 0;
    int count_sommet_instancie_milliards = 0; 
    int count_contraintes_millions = 0;
    int count_contraintes_milliards = 0;
    
    /*Premier sommet passer à la file*/
    memo_pile temp_mem;
    int tmp_valeur;
    temp_mem.sommet_courant = &file_sommet[0];
    temp_mem.indice_domaine = 0;
    temp_mem.nb_sommets_filtres = 0;
    pile_push(pile, temp_mem);

    /*Début compte du temps d'éxecution*/
    
    float clock_start = clock();
    
    /*Boucle de résolution*/
    /*Condition d'arrêt: grille insoluble (pile vide)*/
    
    while (!pile_is_empty(pile)) {

        temp_mem = pile_pop(pile);

        /*Backtrack*/
        
        if (temp_mem.indice_domaine >= temp_mem.sommet_courant->card_domaine) {

            if (temp_mem.sommet_courant->is_known == 0) {
                temp_mem.sommet_courant->valeur = 0;
            }

            /*Si pas de bactrack test sur la valeur du sommet courant*/
        } else {

            tmp_valeur = temp_mem.sommet_courant->domaine[temp_mem.indice_domaine];

            /*Si consistant affectation de la valeur au sommet courant*/
            
            if (is_consistent(temp_mem.sommet_courant, tmp_valeur) == 1) {
                
                 if (count_contraintes_millions == 999999999) {
                    count_contraintes_milliards++;
                    count_contraintes_millions = 0;
                } else {
                    count_contraintes_millions++;
                }
                
                if (count_sommet_instancie_millions == 999999999){
                count_sommet_instancie_milliards++;
                count_sommet_instancie_millions = 0;
            }
            else{
            count_sommet_instancie_millions++;}

                temp_mem.sommet_courant->valeur = tmp_valeur;
                temp_mem.indice_domaine++;
                pile_push(pile, temp_mem);

                if (pile->top != pile->max_size-2) {
                    temp_mem.sommet_courant = &file_sommet[(pile->top) + 1];
                    temp_mem.indice_domaine = 0;
                    pile_push(pile, temp_mem);
                    
                    /*Si la pile équivaut à max_size moins deux c'est que le dernier sommet a été instancié*/
                    
                } else {
                    break;
                }

                /*Si ce n'est pas consistant on prépare la prochaine valeur*/
                
            } else {
                
                if (count_contraintes_millions == 999999999) {
                    count_contraintes_milliards++;
                    count_contraintes_millions = 0;
                } else {
                    count_contraintes_millions++;
                }
                
                temp_mem.indice_domaine++;
                pile_push(pile, temp_mem);
            }
        }
    }

    /*Fin compte du temps d'exécution*/
    
    float clock_end = clock();
    
    /*Affichage des données et stats*/
    
    printf("Temps d'exécution: %f ms\n", 1000 * (clock_end - clock_start) / CLOCKS_PER_SEC);
    printf("Nombre de contraintes: ");
    if(count_contraintes_milliards != 0){
        printf("%d", count_contraintes_milliards);
    }
    printf("%d\n", count_contraintes_millions);
    printf("Nombre de sommets instancies: ");
    if(count_sommet_instancie_milliards != 0){
        printf("%d", count_sommet_instancie_milliards);
    }
    printf("%d\n", count_sommet_instancie_millions);
    printf("\n");
    if (pile_is_empty(pile)) {
        printf("Cette grille n'a pas de solution:\n");
    } else {
        printf("La solution est:\n");
        printf("\n");
        affiche_grille(file_sommet, taille_grille);
    }
};

/*Fonction pour afficher clairement la solution*/
        
void affiche_grille(sommet* file_sommet, int taille_grille) {
    
    /*init*/
    
    int i;
    int curseur;
    int n;
    char ligne[taille_grille * 4 + 1];
    ligne[taille_grille * 4] = '\0';
    char interligne[taille_grille * 4 + 1];
    interligne[taille_grille * 4] = '\0';

    /*Tratement et affichage de la ligne et de l'interligne*/
    /*(La dernière interligne inexistante est créé j'ai considéré que ca ne gêner en rien)*/
    
    for (i = 0; i < taille_grille * taille_grille; i++) {
        
        /*En début de ligne les caractère sont tous initialisés à ' '*/
        if (i % taille_grille == 0) {
            for (n = 0; n < taille_grille * 4; n++) {
                ligne[n] = ' ';
                interligne[n] = ' ';
            }
            /*Le curseur est la position du sommet sur la ligne des sommets*/
            /*(Ainsi que sur les interlignes mais ceci à moins de sens pour la compréhension)*/
            curseur = 0;
        }

        /*Remplacer les espaces qui doivent l'être*/
        
        ligne[curseur] = file_sommet[i].valeur + 48;

        int j;
        for (j = 0; j < file_sommet[i].card_liste_sommet_inf; j++) {
            if (file_sommet[i].liste_sommets_inf[j]->numero_sommet == (file_sommet[i].numero_sommet + 1)) {
                ligne[curseur + 2] = '>';
            }
            if (file_sommet[i].liste_sommets_inf[j]->numero_sommet == (file_sommet[i].numero_sommet + taille_grille)) {
                interligne[curseur] = 'v';
            }
        }

        for (j = 0; j < file_sommet[i].card_liste_sommet_sup; j++) {
            if (file_sommet[i].liste_sommets_sup[j]->numero_sommet == (file_sommet[i].numero_sommet + 1)) {
                ligne[curseur + 2] = '<';
            }
            if (file_sommet[i].liste_sommets_sup[j]->numero_sommet == (file_sommet[i].numero_sommet + taille_grille)) {
                interligne[curseur] = '^';
            }
        }

        if (i % taille_grille != (taille_grille - 1)) {
            
            /*Plus 4 pour l'esthétique, plus deux c'était moche!*/
            
            curseur += 4;
        }
        if (i % taille_grille == (taille_grille - 1)) {
            printf("%s\n", ligne);
            printf("%s\n", interligne);
        }
    }

};
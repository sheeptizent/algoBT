/*jeremie*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#ifndef MODELISE_H
#define	MODELISE_H

typedef struct sommet {
    int is_known; //sommet est reseigné ou non (0 ou 1)
    int numero_sommet; //numéro du sommet équivalent à l'indice du tableau
    int is_assigned; //le sommet est assigné ou non (0 ou 1)
    int valeur; //valeur prise dans le domaine
    double card_domaine; //cardianal du domaine
    int *domaine; //tableau de valeurs du domaine
    int card_liste_sommet_diff; // cardinal des sommets en relation différent de
    struct sommet* *liste_sommets_diff; // liste de pointeur vers les sommets en relation différent de
    int card_liste_sommet_sup; // cardinal des sommets en relation supérieur à
    struct sommet* *liste_sommets_sup; 
    int card_liste_sommet_inf; // cardinal des sommets en relation inférieur à
    struct sommet* *liste_sommets_inf; //liste de pointeur vers les sommets en relation inférieur à
    double coeff_contraintes; // coefficient par pondération des différents types de relations
} sommet;

typedef struct memo_pile {
    int indice_domaine; // où on se trouve dans le choix de la valeur
    sommet* sommet_courant; // le sommet concerné
    int nb_sommets_filtres; // le nombres de sommets filtrés par instanciation d'une valeur sur le sommet concerné
    struct sommet_filtre* liste_sommets_filtres; //liste de struct sommet_filtre
} memo_pile;

typedef struct sommet_filtre { 
    sommet* sommet_filtre; // le sommet filtré
    int nb_valeurs_filtrees; // le nombre de valeurs filtrées
    int* valeurs_filtrees; // tableau des valeurs filtrées
} sommet_filtre;

FILE* ouvrir_fichier(int argc, char * argv1);

void modelisation_donnees(sommet* tab, FILE* file, int taille_grille);

void short_domaine_bt(sommet* lst_sommet, int taille_grille);

#endif	/* MODELISE_H */


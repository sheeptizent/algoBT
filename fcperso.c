/*jeremie*/
#include "fc.h"
#include "modelise.h"
#include "pile.h"
#include "bt.h"
#include "tableaufc.h"
#include "fcperso.h"

/*Fonction qui prepare les valeurs pour la fonction heuristique_cross_perso*/
/*NB on ne recalcule pas pour chaque tour de boucle si il reste des valeurs de la croix à instancier*/

void prepare_cross_perso(int* sommet_num_min_ligne, int* sommet_num_max_ligne, int* num_sommet_cross, int taille_grille) {
    int pos_in_ligne = *num_sommet_cross % taille_grille;
    int diff_sup = taille_grille - 1 - pos_in_ligne;
    *sommet_num_min_ligne = *num_sommet_cross - pos_in_ligne;
    *sommet_num_max_ligne = *num_sommet_cross + diff_sup;
};

/*Fonction heuristique cross_perso de choix de la prochaine variable à instancier*/

sommet* heuristique_cross_perso(sommet* file_sommet, int nombre_sommet, int taille_grille, int *num_sommet_cross, int* flag_cross, int* sommet_num_min_ligne, int* sommet_num_max_ligne) {
    int i;
    *flag_cross = 0;
    sommet* tmp_sommet;
    sommet* tmp_sommet_cross;
    double cmp = 2;
    double cmp_cross = 2;

    for (i = 0; i < nombre_sommet; i++) {

        if (file_sommet[i].is_assigned != 1) {
            double dom_deg = file_sommet[i].card_domaine / file_sommet[i].coeff_contraintes;

            /*Le plus petit coefficient est choisie*/
            /*Coeff_contraintes est pondérées, car une relation de supériorité ou d'infériorité filtre beaucoup plus de valeurs*/

            if (cmp > dom_deg) {
                cmp = dom_deg;
                tmp_sommet = &file_sommet[i];
            }
            
            /*On choisit parmis les valeurs de la croix*/
            
            if (((file_sommet[i].numero_sommet % taille_grille) == *num_sommet_cross % taille_grille) || (*sommet_num_min_ligne <= file_sommet[i].numero_sommet) && (file_sommet[i].numero_sommet < *sommet_num_max_ligne)) {
                *flag_cross = 1;
                if (cmp_cross > dom_deg) {
                    cmp_cross = dom_deg;
                    tmp_sommet_cross = &file_sommet[i];
                }
            }
        }
    }

    /*S'il reste des valeurs sinon le sommet le plus prometteur est désigné*/
    
    if (*flag_cross == 0) {
        *num_sommet_cross = tmp_sommet->numero_sommet;
        return tmp_sommet;
    } else {
        return tmp_sommet_cross;
    }

};

/*Fonction de résolution de la grille futoshiki avec Forward Checking heuristique cross_perso*/

/*Pour tester FC en mode simple ou mode heuristique dom/deg, décommenter les lignes correspondantes*/

void resolve_fut_fc_perso(sommet* file_sommet, pile* pile, int taille_grille) {

    /*init*/

    int count_sommet_instancie_millions = 0;
    int count_sommet_instancie_milliards = 0;
    int count_filtrage_millions = 0;
    int count_filtrage_milliards = 0;
    int nb_sommet = taille_grille*taille_grille;
    int indice_domaine_courant = 0;
    int tmp_valeur = 0;
    int flag_cross;
    int num_sommet_cross;
    int sommet_num_min_ligne;
    int sommet_num_max_ligne;
    flag_cross = 1;

    /*Allocation mémoire pour mémo_pile tampon*/

    memo_pile temp_mem;
    temp_mem.sommet_courant = heuristique_dom_s_deg(file_sommet, nb_sommet); //&file_sommet[0];
    num_sommet_cross = temp_mem.sommet_courant->numero_sommet;
    prepare_cross_perso(&sommet_num_min_ligne, &sommet_num_max_ligne, &num_sommet_cross, taille_grille);
    temp_mem.indice_domaine = indice_domaine_courant;
    temp_mem.nb_sommets_filtres = 0;
    temp_mem.liste_sommets_filtres = malloc(sizeof (struct sommet_filtre) * 2 * (taille_grille - 1));
    int init;
    for (init = 0; init < ((taille_grille * 2) - 2); init++) {
        int * tab_int = malloc(sizeof (int) *taille_grille);
        temp_mem.liste_sommets_filtres[init].valeurs_filtrees = tab_int;
    }

    pile_push(pile, temp_mem);

    /*Début compte du temps d'éxecution*/

    float clock_start = clock();

    /*Boucle de résolution*/
    /*Condition d'arrêt: grille insoluble (pile vide)*/

    while (!pile_is_empty(pile)) {

        temp_mem = pile_pop(pile);

        /*Si temp_mem n'a pas été "réinitialiser" à la fin du tour précédent c'est que l'on vient d'un backtrack ou d'un filtrage avec un domaine vide
         on restaure donc les dommaine*/
        /*NB: La fonction ne fait rien pour un "nouveau" sommet car le cardianal de sa liste de sommets filtrés est de 0*/

        restauration_domaine(&temp_mem);

        /*Bactrack*/

        if (temp_mem.indice_domaine >= temp_mem.sommet_courant->card_domaine) {
            temp_mem.sommet_courant->is_assigned = 0;

            if (temp_mem.sommet_courant->is_known == 0) {
                temp_mem.sommet_courant->valeur = 0;
            }

        } else {

            /*Récupération de la valeur suivante grâce à l'indice*/

            tmp_valeur = valeur_peek_fc(temp_mem.sommet_courant->domaine, taille_grille, temp_mem.indice_domaine);

            /*Si le filtrage renvoie 0 on prépare la valeur suivante*/

            if (!filtrage(tmp_valeur, &temp_mem, taille_grille)) {

                if (count_filtrage_millions == 999999999) {
                    count_filtrage_milliards++;
                    count_filtrage_millions = 0;
                } else {
                    count_filtrage_millions++;
                }

                temp_mem.indice_domaine++;
                pile_push(pile, temp_mem);

                /*Si le filtrage renvoie 1 on instancie le sommet*/

            } else {

                if (count_filtrage_millions == 999999999) {
                    count_filtrage_milliards++;
                    count_filtrage_millions = 0;
                } else {
                    count_filtrage_millions++;
                }

                if (count_sommet_instancie_millions == 999999999) {
                    count_sommet_instancie_milliards++;
                    count_sommet_instancie_millions = 0;
                } else {
                    count_sommet_instancie_millions++;
                }

                temp_mem.sommet_courant->valeur = tmp_valeur;
                temp_mem.sommet_courant->is_assigned = 1;
                temp_mem.indice_domaine++;
                pile_push(pile, temp_mem);

                if (pile->top != pile->max_size - 2) {
                    if (flag_cross == 0) {
                        prepare_cross_perso(&sommet_num_min_ligne, &sommet_num_max_ligne, &num_sommet_cross, taille_grille);
                    }
                    temp_mem.sommet_courant = heuristique_cross_perso(file_sommet, nb_sommet, taille_grille, &num_sommet_cross, &flag_cross, & sommet_num_min_ligne, &sommet_num_max_ligne); //heuristique_dom_s_deg(file_sommet, nb_sommet); //&file_sommet[temp_mem.sommet_courant->numero_sommet + 1];
                    temp_mem.indice_domaine = 0;
                    temp_mem.nb_sommets_filtres = 0;
                    pile_push(pile, temp_mem);

                    /*Si la pile équivaut à max_size moins deux c'est que le dernier sommet a été instancié*/

                } else {
                    break;
                }
            }
        }
    }

    /*Fin compte du temps d'exécution*/

    float clock_end = clock();

    /*Affichage des données et stats*/


    printf("Temps d'exécution: %f ms\n", 1000 * (clock_end - clock_start) / CLOCKS_PER_SEC);
    printf("Nombre de filtrages: ");
    if (count_filtrage_milliards != 0) {
        printf("%d", count_filtrage_milliards);
    }
    printf("%d\n", count_filtrage_millions);
    printf("Nombre de sommets instancies: ");
    if (count_sommet_instancie_milliards != 0) {
        printf("%d", count_sommet_instancie_milliards);
    }
    printf("%d\n", count_sommet_instancie_millions);
    printf("\n");
    if (pile_is_empty(pile)) {
        printf("Cette grille n'a pas de solution:\n");
    } else {
        printf("La solution est:\n");
        printf("\n");
        affiche_grille(file_sommet, taille_grille);
    }
};

/*jeremie*/
#include "fc.h"
#include "modelise.h"
#include "pile.h"
#include "bt.h"
#include "tableaufc.h"

/*Fonction de filtrage des dommaines par rapport à une valeur*/

/*Retourne 1 si les domaines des sommets filtrés ne sont pas vide, retourne 0 sinon*/

int filtrage(int valeur, memo_pile* memo_pile, int taille_grille) {

    /*init*/

    int card_diff = memo_pile->sommet_courant->card_liste_sommet_diff;
    int card_inf = memo_pile->sommet_courant->card_liste_sommet_inf;
    int card_sup = memo_pile->sommet_courant->card_liste_sommet_sup;

    int it = 0;
    int j;
    int k;
    int i;

    /*Filtrage du domaine des sommets de la liste d'adjacences de relation inférieur*/

    for (j = 0; j < card_inf; j++) {

        /*On ne filtre que les sommet non instanciés ou on renseignés*/

        if ((memo_pile->sommet_courant->liste_sommets_inf[j]->is_known != 1) && (memo_pile->sommet_courant->liste_sommets_inf[j]->is_assigned != 1)) {
            int jp;
            int itinf = 0;
            int traite = 0;
            int nb_valeurs = 0;
            for (jp = valeur - 1; jp < taille_grille; jp++) {
                if (memo_pile->sommet_courant->liste_sommets_inf[j]->domaine[jp] != 0) {
                    if (memo_pile->sommet_courant->liste_sommets_inf[j]->domaine[jp] != 0) {
                        traite = 1;
                        memo_pile->sommet_courant->liste_sommets_inf[j]->card_domaine--;
                        memo_pile->liste_sommets_filtres[it].sommet_filtre = memo_pile->sommet_courant->liste_sommets_inf[j];
                        nb_valeurs++;
                        memo_pile->liste_sommets_filtres[it].nb_valeurs_filtrees = nb_valeurs;
                        memo_pile->liste_sommets_filtres[it].valeurs_filtrees[itinf] = memo_pile->sommet_courant->liste_sommets_inf[j]->domaine[jp];
                        memo_pile->sommet_courant->liste_sommets_inf[j]->domaine[jp] = 0;
                        itinf++;
                    }
                }
            }
            if (traite == 1) {
                memo_pile->nb_sommets_filtres = ++it;
            }

            /*Si le domaine du sommet filtré est vide on retourne directement 0*/

            if ((memo_pile->sommet_courant->liste_sommets_inf[j]->card_domaine) == 0) {
                return 0;
            }
        }
    }

    /*Filtrage du domaine des sommets de la liste d'adjacences de relation supérieur*/
    /*Même principes*/

    for (k = 0; k < card_sup; k++) {
        if ((memo_pile->sommet_courant->liste_sommets_sup[k]->is_known != 1) && (memo_pile->sommet_courant->liste_sommets_sup[k]->is_assigned != 1)) {
            int kp;
            int itsup = 0;
            int traite = 0;
            int nb_valeurs = 0;
            for (kp = 0; kp < valeur; kp++) {
                if (memo_pile->sommet_courant->liste_sommets_sup[k]->domaine[kp] != 0) {
                    if (memo_pile->sommet_courant->liste_sommets_sup[k]->domaine[kp] != 0) {
                        traite = 1;
                        memo_pile->sommet_courant->liste_sommets_sup[k]->card_domaine--;
                        memo_pile->liste_sommets_filtres[it].sommet_filtre = memo_pile->sommet_courant->liste_sommets_sup[k];
                        nb_valeurs++;
                        memo_pile->liste_sommets_filtres[it].nb_valeurs_filtrees = nb_valeurs;
                        memo_pile->liste_sommets_filtres[it].valeurs_filtrees[itsup] = memo_pile->sommet_courant->liste_sommets_sup[k]->domaine[kp];
                        memo_pile->sommet_courant->liste_sommets_sup[k]->domaine[kp] = 0;
                        itsup++;
                    }
                }
            }
            if (traite == 1) {
                memo_pile->nb_sommets_filtres = ++it;
            }
            if ((memo_pile->sommet_courant->liste_sommets_sup[k]->card_domaine) == 0) {
                return 0;
            }
        }
    }

    /*Filtrage du domaine des sommets de la liste d'adjacences de relation différent*/
    /*Même principes*/

    for (i = 0; i < card_diff; i++) {
        if ((memo_pile->sommet_courant->liste_sommets_diff[i]->is_known != 1) && (memo_pile->sommet_courant->liste_sommets_diff[i]->is_assigned != 1)) {
            if (valeur == memo_pile->sommet_courant->liste_sommets_diff[i]->domaine[valeur - 1]) {
                if (memo_pile->sommet_courant->liste_sommets_diff[i]->card_domaine - 1 == 0) {
                    return 0;
                } else {
                    memo_pile->sommet_courant->liste_sommets_diff[i]->card_domaine--;
                    memo_pile->sommet_courant->liste_sommets_diff[i]->domaine[valeur - 1] = 0;
                    memo_pile->liste_sommets_filtres[it].sommet_filtre = memo_pile->sommet_courant->liste_sommets_diff[i];
                    memo_pile->liste_sommets_filtres[it].nb_valeurs_filtrees = 1;
                    memo_pile->liste_sommets_filtres[it].valeurs_filtrees[0] = valeur;
                    memo_pile->nb_sommets_filtres = ++it;
                }

            }
        }
    }
    return 1;
};

/*Fonction de restauration d'un domaine*/

void restauration_domaine(memo_pile* memo_pile) {

    int it;
    for (it = 0; it < memo_pile->nb_sommets_filtres; it++) {
        int its;
        for (its = 0; its < memo_pile->liste_sommets_filtres[it].nb_valeurs_filtrees; its++) {
            int valeur = memo_pile->liste_sommets_filtres[it].valeurs_filtrees[its];
            memo_pile->liste_sommets_filtres[it].sommet_filtre->domaine[valeur - 1] = valeur;
            memo_pile->liste_sommets_filtres[it].sommet_filtre->card_domaine++;
        }
        memo_pile->liste_sommets_filtres[it].nb_valeurs_filtrees = 0;
    }
    memo_pile->nb_sommets_filtres = 0;
};

/*Fonction qui retourne la prochaine valeur à tester*/

/*Diffère de BT cette fonctionnalité est implémenté directement dans resolve BT car ici le tableau n'est pas réorganisé pour gagner du temps lors de la restauration des données
  et ne pas sauvegarder la position de la valeur filtrées dans la structure mémo_pile*/

int valeur_peek_fc(int* domaine, int taille_grille, int indice) {
    int count = 0;
    int i;
    for (i = 0; i < taille_grille; i++) {
        if (domaine[i] != 0) {
            if (count == indice) {
                return domaine[i];
            } else {
                count++;
            }
        }
    }
    return 0;
};

/*Fonction heuristique dom/deg de choix de la prochaine variable à instancier*/

sommet* heuristique_dom_s_deg(sommet* file_sommet, int nombre_sommet) {
    int i;
    sommet* tmp_sommet;
    double cmp = 2;
    for (i = 0; i < nombre_sommet; i++) {
        if (file_sommet[i].is_assigned != 1) {
            double dom_deg = file_sommet[i].card_domaine / file_sommet[i].coeff_contraintes;

            /*Le plus petit coefficient est choisie*/
            /*Coeff_contraintes est pondérées, car une relation de supériorité ou d'infériorité filtre beaucoup plus de valeurs*/

            if (cmp > dom_deg) {
                cmp = dom_deg;
                tmp_sommet = &file_sommet[i];
            }
        }
    }
    return tmp_sommet;
};

/*Fonction de résolution de la grille futoshiki avec Forward Checking heuristique cross_perso*/

/*Pour tester FC en mode simple ou mode heuristique dom/deg, décommenter les lignes correspondantes*/

void resolve_fut_fc(sommet* file_sommet, pile* pile, int taille_grille) {

    /*init*/
   
    int count_sommet_instancie_millions = 0;
    int count_sommet_instancie_milliards = 0;
    int count_filtrage_millions = 0;
    int count_filtrage_milliards = 0;
    int nb_sommet = taille_grille*taille_grille;
    int indice_domaine_courant = 0;
    int tmp_valeur = 0;

    /*Allocation mémoire pour mémo_pile tampon*/

    memo_pile temp_mem;
    temp_mem.sommet_courant = heuristique_dom_s_deg(file_sommet, nb_sommet); //&file_sommet[0];
    temp_mem.indice_domaine = indice_domaine_courant;
    temp_mem.nb_sommets_filtres = 0;
    temp_mem.liste_sommets_filtres = malloc(sizeof (struct sommet_filtre) * 2 * (taille_grille - 1));
    int init;
    for (init = 0; init < ((taille_grille * 2) - 2); init++) {
        int * tab_int = malloc(sizeof (int) *taille_grille);
        temp_mem.liste_sommets_filtres[init].valeurs_filtrees = tab_int;
    }

    pile_push(pile, temp_mem);

    /*Début compte du temps d'éxecution*/

    float clock_start = clock();

    /*Boucle de résolution*/
    /*Condition d'arrêt: grille insoluble (pile vide)*/

    while (!pile_is_empty(pile)) {

        temp_mem = pile_pop(pile);

        /*Si temp_mem n'a pas été "réinitialiser" à la fin du tour précédent c'est que l'on vient d'un backtrack ou d'un filtrage avec un domaine vide
         on restaure donc les dommaine*/
        /*NB: La fonction ne fait rien pour un "nouveau" sommet car le cardianal de sa liste de sommets filtrés est de 0*/

        restauration_domaine(&temp_mem);

        /*Bactrack*/

        if (temp_mem.indice_domaine >= temp_mem.sommet_courant->card_domaine) {
            temp_mem.sommet_courant->is_assigned = 0;

            if (temp_mem.sommet_courant->is_known == 0) {
                temp_mem.sommet_courant->valeur = 0;
            }

        } else {

            /*Récupération de la valeur suivante grâce à l'indice*/

            tmp_valeur = valeur_peek_fc(temp_mem.sommet_courant->domaine, taille_grille, temp_mem.indice_domaine);

            /*Si le filtrage renvoie 0 on prépare la valeur suivante*/

            if (!filtrage(tmp_valeur, &temp_mem, taille_grille)) {

                if (count_filtrage_millions == 999999999) {
                    count_filtrage_milliards++;
                    count_filtrage_millions = 0;
                } else {
                    count_filtrage_millions++;
                }

                temp_mem.indice_domaine++;
                pile_push(pile, temp_mem);

                /*Si le filtrage renvoie 1 on instancie le sommet*/

            } else {

                if (count_filtrage_millions == 999999999) {
                    count_filtrage_milliards++;
                    count_filtrage_millions = 0;
                } else {
                    count_filtrage_millions++;
                }

                if (count_sommet_instancie_millions == 999999999) {
                    count_sommet_instancie_milliards++;
                    count_sommet_instancie_millions = 0;
                } else {
                    count_sommet_instancie_millions++;
                }

                temp_mem.sommet_courant->valeur = tmp_valeur;
                temp_mem.sommet_courant->is_assigned = 1;
                temp_mem.indice_domaine++;
                pile_push(pile, temp_mem);

                if (pile->top != pile->max_size - 2) {                       
                    temp_mem.sommet_courant = heuristique_dom_s_deg(file_sommet, nb_sommet);//&file_sommet[temp_mem.sommet_courant->numero_sommet + 1];
                    temp_mem.indice_domaine = 0;
                    temp_mem.nb_sommets_filtres = 0;
                    pile_push(pile, temp_mem);

                    /*Si la pile équivaut à max_size moins deux c'est que le dernier sommet a été instancié*/

                } else {
                    break;
                }
            }
        }
    }

    /*Fin compte du temps d'exécution*/

    float clock_end = clock();

    /*Affichage des données et stats*/


    printf("Temps d'exécution: %f ms\n", 1000 * (clock_end - clock_start) / CLOCKS_PER_SEC);
    printf("Nombre de filtrages: ");
    
    if (count_filtrage_milliards != 0) {
        printf("%d", count_filtrage_milliards);
    }
    printf("%d\n", count_filtrage_millions);
    printf("Nombre de sommets instancies: ");
    if (count_sommet_instancie_milliards != 0) {
        printf("%d", count_sommet_instancie_milliards);
    }
    printf("%d\n", count_sommet_instancie_millions);
    printf("\n");
    if (pile_is_empty(pile)) {
        printf("Cette grille n'a pas de solution:\n");
    } else {
        printf("La solution est:\n");
        printf("\n");
        affiche_grille(file_sommet, taille_grille);
    }
};
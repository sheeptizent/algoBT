/*jeremie*/
#include "modelise.h"
#include "pile.h"
#include "bt.h"
#include "tableaufc.h"

/*Fonction qui permet d'allouer la mémoire nécessaire au tableau de sommets*/

void tableau_de_sommets_init(sommet* tab, int taille_grille) {
    int i;

    for (i = 0; i < (taille_grille * taille_grille); i++) {

        int* tab_ints;
        tab_ints = malloc(sizeof (int) * taille_grille);
        if (tab_ints == NULL) {
            fprintf(stderr, "Probleme allocation memoire durant la création de la liste de sommet");
            exit(EXIT_FAILURE);
        }

        struct sommet** liste_p_sommet_diff;
        liste_p_sommet_diff = malloc(sizeof (sommet*) * (taille_grille - 1) * 2);
        if (liste_p_sommet_diff == NULL) {
            fprintf(stderr, "Probleme allocation memoire durant la création de la liste de sommet");
            exit(EXIT_FAILURE);
        }

        struct sommet** liste_p_sommet_inf;
        liste_p_sommet_inf = malloc(sizeof (sommet*) * 4);
        if (liste_p_sommet_inf == NULL) {
            fprintf(stderr, "Probleme allocation memoire durant la création de la liste de sommet");
            exit(EXIT_FAILURE);
        }

        struct sommet** liste_p_sommet_sup;
        liste_p_sommet_sup = malloc(sizeof (sommet*) * 4);
        if (liste_p_sommet_sup == NULL) {
            fprintf(stderr, "Probleme allocation memoire durant la création de la liste de sommet");
            exit(EXIT_FAILURE);
        }

        tab[i].is_known = 0;
        tab[i].numero_sommet = i;
        tab[i].is_assigned = 0;
        tab[i].valeur = 0;
        tab[i].card_domaine = taille_grille;
        tab[i].card_liste_sommet_diff = 2 * (taille_grille - 1);
        tab[i].card_liste_sommet_inf = 0;
        tab[i].card_liste_sommet_sup = 0;
        tab[i].coeff_contraintes = 0;

        int j;
        for (j = 0; j < taille_grille; j++) {
            tab_ints[j] = j + 1;
        }

        tab[i].domaine = tab_ints;
        tab[i].liste_sommets_diff = liste_p_sommet_diff;
        tab[i].liste_sommets_inf = liste_p_sommet_inf;
        tab[i].liste_sommets_sup = liste_p_sommet_sup;
    }


};

/*Fonction qui met le sommet désiré en fin de tableau*/

void enlever_p_sommet(sommet** tab, int taille_tab, sommet* s_cutoff) {

    int i;
    int flag = 0;
    for (i = 0; i < taille_tab; i++) {
        if (flag == 1) {
            tab[i - 1] = tab[i];
        } else {
            if (tab[i] == s_cutoff) {
                flag = 1;
            }
        }
    }
};

/*Fonction qui libère la mémoire allouée au tableau*/

void tableau_de_sommets_destroy(sommet* tab, int taille_grille) {
    int i = 0;
    for (i = 0; i < taille_grille * taille_grille; i++) {
        tab[i].liste_sommets_diff = NULL;
        tab[i].liste_sommets_inf = NULL;
        tab[i].liste_sommets_sup = NULL;
        tab[i].domaine = NULL;
        free(tab[i].liste_sommets_diff);
        free(tab[i].liste_sommets_inf);
        free(tab[i].liste_sommets_sup);
        free(tab[i].domaine);
    }
    tab = NULL;
    free(tab);
};


/*jeremie*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include "pile.h"
#include "modelise.h"

#ifndef FCPERSO_H
#define	FCPERSO_H

sommet* heuristique_cross_perso(sommet* file_sommet, int nombre_sommet, int taille_grille, int* num_sommet_cross, int* flag_cross, int* sommet_num_min_ligne, int* sommet_num_max_ligne);

void prepare_cross_perso(int* sommet_num_min_ligne, int* sommet_num_max_ligne, int* num_sommet_cross, int taille_grille);

void resolve_fut_fc_perso(sommet* file_sommet, pile* pile, int taille_grille);

#endif	/* FCPERSO_H */


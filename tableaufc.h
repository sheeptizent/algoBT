/*jeremie*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include "modelise.h"

#ifndef TABLEAUFC_H
#define	TABLEAUFC_H

void tableau_de_sommets_init(sommet* tab, int taille_grille);

void enlever_p_sommet(sommet** tab, int taille_tab, sommet* s_cutoff);

void tableau_de_sommets_destroy(sommet* tab, int taille_grille);

#endif	/* TABLEAUFC_H */

